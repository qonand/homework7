# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework7.git`
2. Run `docker-compose up -d` in the project folder

# How to test the application
Open one of the following urls in your web browser

 - http://localhost/3eda0e6192c071bafa76332db5ac51dc.png
 - http://localhost/5de30258f15ef03fa63a604a5fcaaf36.png
 - http://localhost/aa93b8ad22b2b4385e9515b7809cddd3.png
 - http://localhost/d026bf1f716100486861abab6cf0840a.png